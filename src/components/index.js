import Vue from 'vue'
import FadeTransition from './transition/FadeTransition'
import PageTransition from './transition/PageTransition'

// Components that are registered global.
;[FadeTransition, PageTransition].forEach(Component => {
  Vue.component(Component.name, Component)
})
