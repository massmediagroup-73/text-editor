import Vue from 'vue'
import { library } from '@fortawesome/fontawesome-svg-core'
import {
  faAlignCenter,
  faAlignLeft,
  faAlignRight,
  faAlignJustify,
  faBold,
  faItalic,
  faUnderline,
  faChevronDown,
  faImage,
} from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

library.add(
  faAlignCenter,
  faAlignLeft,
  faAlignRight,
  faAlignJustify,
  faBold,
  faItalic,
  faUnderline,
  faChevronDown,
  faImage
)

Vue.component('font-awesome-icon', FontAwesomeIcon)
