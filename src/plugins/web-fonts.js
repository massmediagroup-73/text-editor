import WebFont from 'webfontloader'
import { getGoogleFonts } from '@/utils/fonts'

const loadWebFonts = () =>
  WebFont.load({
    google: {
      families: getGoogleFonts().map(font => font.name),
    },
    classes: false,
    events: false,
  })
loadWebFonts()
