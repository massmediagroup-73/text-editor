export default [
  {
    path: '/',
    name: 'home',
    component: () =>
      import(/* webpackChunkName: "index" */ '../views/home/Index.vue'),
  },
]
