import * as types from '../mutation-types'

export const state = {
  image: '',
}

export const mutations = {
  [types.SET_MEDIA](state, payload) {
    state.image = payload
  },
}

export const actions = {
  setImage({ commit }, payload) {
    commit(types.SET_MEDIA, payload)
  },
}
