import * as types from '../mutation-types'

export const state = {
  align: '',
}

export const mutations = {
  [types.SET_TEXT_ALIGN](state, payload) {
    state.align = payload || 'initial'
  },
}

export const actions = {
  setTextAlign({ commit }, payload) {
    commit(types.SET_TEXT_ALIGN, payload)
  },
}
