import * as types from '../mutation-types'

export const state = {
  fontWeight: '',
  fontStyle: '',
  textDecoration: '',
}

export const mutations = {
  [types.SET_FONT_WEIGHT](state, payload) {
    state.fontWeight = payload || 'normal'
  },
  [types.SET_FONT_STYLE](state, payload) {
    state.fontStyle = payload || 'normal'
  },
  [types.SET_TEXT_DECORATION](state, payload) {
    state.textDecoration = payload || 'none'
  },
}

export const actions = {
  setFontWeight({ commit }, payload) {
    commit(types.SET_FONT_WEIGHT, payload)
  },
  setFontStyle({ commit }, payload) {
    commit(types.SET_FONT_STYLE, payload)
  },
  setTextDecoration({ commit }, payload) {
    commit(types.SET_TEXT_DECORATION, payload)
  },
}
