import * as types from '../mutation-types'
import { getFonts, getDefaultFont } from '@/utils/fonts'
import { getFontSizes, getDefaultFontSize } from '@/utils/fontSizes'

export const state = {
  fonts: getFonts(),
  selectedFont: getDefaultFont(),
  textColor: '',
  fontSizes: getFontSizes(),
  selectedFontSize: getDefaultFontSize(),
}

export const mutations = {
  [types.SET_FONT](state, payload) {
    state.selectedFont = payload
  },
  [types.SET_COLOR](state, payload) {
    state.textColor = payload
  },
  [types.SET_FONT_SIZE](state, payload) {
    state.selectedFontSize = payload
  },
}

export const actions = {
  setFont({ commit }, payload) {
    commit(types.SET_FONT, payload)
  },
  setTextColor({ commit }, payload) {
    commit(types.SET_COLOR, payload)
  },
  setFontSize({ commit }, payload) {
    commit(types.SET_FONT_SIZE, payload)
  },
}
