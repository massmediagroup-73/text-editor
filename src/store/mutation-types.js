// editorTextStyle.js
export const SET_FONT = 'SET_FONT'
export const SET_COLOR = 'SET_COLOR'
export const SET_FONT_SIZE = 'SET_FONT_SIZE'

// editorTextAlign.js
export const SET_TEXT_ALIGN = 'SET_TEXT_ALIGN'

// editorTextFormatting.js
export const SET_FONT_WEIGHT = 'SET_FONT_WEIGHT'
export const SET_FONT_STYLE = 'SET_FONT_STYLE'
export const SET_TEXT_DECORATION = 'SET_TEXT_DECORATION'

// editorMedia.js
export const SET_MEDIA = 'SET_MEDIA'
