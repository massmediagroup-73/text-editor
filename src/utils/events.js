export const throttle = (callback, limit) => {
  let wait = false

  return function() {
    if (!wait) {
      callback.call()
      wait = true
      setTimeout(function() {
        wait = false
      }, limit)
    }
  }
}

export const debounce = (callback, delay) => {
  let timerId

  return function(...args) {
    if (timerId) {
      clearTimeout(timerId)
    }
    timerId = setTimeout(() => {
      callback(...args)
      timerId = null
    }, delay)
  }
}
