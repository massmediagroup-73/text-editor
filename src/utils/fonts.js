const fonts = [
  {
    name: 'Arial',
    default: true,
    source: 'system',
  },
  {
    name: 'Courier',
    default: false,
    source: 'system',
  },
  {
    name: 'Mansalva',
    default: false,
    source: 'google',
  },
  {
    name: 'Roboto',
    default: false,
    source: 'google',
  },
  {
    name: 'Times New Roman',
    default: false,
    source: 'system',
  },
  {
    name: 'Vibes',
    default: false,
    source: 'google',
  },
]

export const getFonts = () => fonts
export const getDefaultFont = () => fonts.find(font => font.default)
export const getGoogleFonts = () =>
  fonts.filter(font => font.source === 'google')
