const path = require('path')

module.exports = {
  productionSourceMap: false,
  integrity: true,
  configureWebpack: () => {
    if (process.env.NODE_ENV === 'production') {
      return {
        devtool: 'source-map',
      }
    } else {
      return {
        devtool: 'source-map',
      }
    }
  },

  pluginOptions: {
    'style-resources-loader': {
      patterns: [path.resolve(__dirname, 'src/styles/settings/*.scss')],
      preProcessor: 'scss',
    },
  },

  pwa: {
    appleMobileWebAppStatusBarStyle: '#fff',
    name: 'Text Editor',
    themeColor: '#fff',
    workboxPluginMode: 'InjectManifest',
    workboxOptions: {
      swSrc: 'src/service-worker.js',
      exclude: [/\.map$/, /manifest\.json$/],
    },
  },
}
